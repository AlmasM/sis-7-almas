# API rabbitmq support with ZeebeClient
API now can start workflow with the help of the rabbitmq using the route `localhost:8080/order/create-async` this endpoint will produce message that will be consumed by zeebeClient. Zeebe client will start the workflow using the content of produced message.  
<br/>
The same thing is implemented in `feature/kafka` branch with kafka instead of rabbit