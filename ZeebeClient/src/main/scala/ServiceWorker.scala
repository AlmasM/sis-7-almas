import Actors.Order.{CreateOrder, RejectOrder}
import Actors.{DeliveryType, Order, PathGenerator}
import akka.Done
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.AskPattern._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.util.Timeout
import io.zeebe.client.ZeebeClient
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.{JobClient, JobHandler}
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import com.typesafe.config.Config
import spray.json._

import scala.concurrent.Future
import scala.util.{Failure, Success}

class ServiceWorker (zeebeClient: ZeebeClient, config: Config)(implicit val system: ActorSystem[_]) extends JobHandler{
  implicit val timeout: Timeout = Timeout.create(system.settings.config.getDuration("http-duration"))
  implicit val executionContext = system.executionContext
  case class CreatedOrder(id:String, address:String, details:String, userId: String)
  case class OptimizedOrder(id:String, address:String, details:String, userId: String, distance: Float)
  implicit val createResponseFormat = jsonFormat4(CreatedOrder)
  implicit val optimizedOrderFormat = jsonFormat5(OptimizedOrder)

  def createOrder(job:ActivatedJob): Future[HttpResponse] = {
    ActorSystem(Order(config), "creator").ask(CreateOrder(job,_))
  }

  def generateOptimalDistance(job: ActivatedJob):Future[HttpResponse] = {
    ActorSystem(PathGenerator(config), "generator").ask(PathGenerator.Generate(job, _))
  }

  def rejectDelivery(job: ActivatedJob):Future[Done] = {
    ActorSystem(Order(config), "generator").ask(RejectOrder(job, _))
  }

  override def handle(client: JobClient, job: ActivatedJob): Unit = {
    job.getType match {
      case "create-order" =>
        createOrder(job) onComplete{
          case Success(response) =>
            Unmarshal(response).to[CreatedOrder].map(response => {
              system.log.info(s"unmarshalled and got the response ${response.toString}")
              client.newCompleteCommand(job.getKey).variables(response.toJson.toString)
                .send().join()
            })
          case Failure(e) =>
            system.log
            .info(s"failed with exception ${e.getMessage()}")
        }
      case "generate-optimal" =>
        generateOptimalDistance(job) onComplete {
          case Success(response) =>
            Unmarshal(response).to[OptimizedOrder].map(response => {
            system.log
              .info(s"the optimized distance that generated from api is ${response.distance}")
              client.newCompleteCommand(job.getKey).variables(response.toJson.toString)
                .send().join()
              zeebeClient.newPublishMessageCommand()
                .messageName("path-optimized")
                .correlationKey(response.userId)
                .send()
                .join()
          })
          case Failure(e) =>
            system.log
              .info(s"failed with exception ${e.getMessage()}")
        }
      case "reject-delivery" =>
        rejectDelivery(job) onComplete {
          case Success(_) =>
            val orderId = job.getVariablesAsMap.get("id")
                .asInstanceOf[String]
            system.log
              .info(s"rejected delivery of order with id=$orderId")
            client.newCompleteCommand(job.getKey)
              .send().join()
          case Failure(e) =>
            system.log
              .info(s"failed with exception ${e.getMessage()}")
        }
      case "init-free" =>
        ActorSystem(DeliveryType.Free(client, job), "free") ! DeliveryType.DeliverPackage("Almas")
      case "init-standart" =>
        ActorSystem(DeliveryType.Standart(client, job), "standart") ! DeliveryType.DeliverPackage("Daulet")
      case "init-express" =>
        ActorSystem(DeliveryType.Express(client, job), "express") ! DeliveryType.DeliverPackage("Vasya")
    }
  }
}