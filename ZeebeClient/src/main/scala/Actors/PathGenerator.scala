package Actors

import Actors.PathGenerator.PathGeneratorMsg
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse, Uri}
import com.typesafe.config.Config
import io.zeebe.client.api.response.ActivatedJob

import scala.concurrent.duration._
import scala.concurrent.Await


object PathGenerator {
  abstract class PathGeneratorMsg
  final case class Generate(job: ActivatedJob, replyTo: ActorRef[HttpResponse]) extends PathGeneratorMsg
  def apply(config: Config): Behavior[PathGeneratorMsg] =
    Behaviors.setup(context => new PathGenerator(context, config))
}

class PathGenerator (context: ActorContext[PathGeneratorMsg], config: Config)
  extends AbstractBehavior[PathGeneratorMsg](context){
  implicit val system = context.system
  implicit val executionContext = system.executionContext

  override def onMessage(msg: PathGeneratorMsg): Behavior[PathGeneratorMsg] = {
    msg match {
      case PathGenerator.Generate(job, replyTo) =>
        val orderId =  job
          .getVariablesAsMap
          .get("id").
          asInstanceOf[String]

        val responseFuture = Http().singleRequest(HttpRequest(
            method = HttpMethods.POST,
            uri = Uri(config.getString("api.optimizeDistanceUri"))
              .withQuery(Query("orderId" -> orderId))
          )
        )
        responseFuture.map(
          response =>
            replyTo ! response
        )
        try {
          Await.result(responseFuture, 5.seconds)
        }catch{
          case e: Throwable => system.log
            .info(s"something went wrong, nothing returned after 5 seconds ${e.getMessage}")
        }
        Behaviors.stopped
      case _ =>
        system.log
        .info("unexpected message")
        Behaviors.stopped
    }
  }
}