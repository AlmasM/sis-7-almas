package Actors
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.JobClient


abstract class Delivery

object DeliveryType {
  final case class DeliverPackage(distance:String) extends Delivery
  object Free {
    def apply(client: JobClient, job: ActivatedJob): Behavior[Delivery] ={
      Behaviors.setup(context => new Free(context, client, job))
    }
  }
  class Free(context:ActorContext[Delivery], client: JobClient, job: ActivatedJob) extends AbstractBehavior[Delivery](context){

    override def onMessage(msg: Delivery): Behavior[Delivery] = {
      msg match {
        case DeliverPackage(_) =>
          context.system
          .log.info(s"Initiating free delivery for id=${job.getVariablesAsMap.get("id")}")
          client.newCompleteCommand(job.getKey)
            .send()
            .join()
          context.system
          .log.info("free delivery initiated")
          Behaviors.stopped
        case _ =>
          context.system
          .log.info("Unexpected message")
          Behaviors.stopped
      }
    }
  }

  object Standart{
    def apply(client: JobClient, job: ActivatedJob): Behavior[Delivery] ={
      Behaviors.setup(context => new Standart(context, client, job))
    }
  }
  class Standart (context:ActorContext[Delivery], client: JobClient, job: ActivatedJob) extends AbstractBehavior[Delivery](context){
    override def onMessage(msg: Delivery): Behavior[Delivery] = {
      msg match {
        case DeliverPackage(_) =>
          context.system
            .log.info(s"Initiating standart delivery for id=${job.getVariablesAsMap.get("id")}")
          client.newCompleteCommand(job.getKey)
            .send()
            .join()
          context.system
            .log.info("standart delivery initiated")
          Behaviors.stopped
        case _ =>
          context.system
            .log.info("unexpected message")
          Behaviors.stopped
      }
    }
  }

  object Express{
    def apply(client: JobClient, job: ActivatedJob): Behavior[Delivery] ={
      Behaviors.setup(context => new Express(context, client, job))
    }
  }
  class Express (context:ActorContext[Delivery], client: JobClient, job: ActivatedJob) extends AbstractBehavior[Delivery](context){
    override def onMessage(msg: Delivery): Behavior[Delivery] = {
      msg match {
        case DeliverPackage(_) =>
          context.system
            .log.info(s"Initiating express delivery for id=${job.getVariablesAsMap.get("id")}")
          client.newCompleteCommand(job.getKey)
            .send()
            .join()
          context.system
            .log.info("express delivery initiated")
          Behaviors.stopped
        case _ =>
          context.system
            .log.info("unexpected message")
          Behaviors.stopped
      }
    }
  }
}