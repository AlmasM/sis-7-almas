package Actors

import Actors.Client.{Command, StartWorkflow}
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import io.zeebe.client.ZeebeClient
import scala.jdk.CollectionConverters._

object Client{
  abstract class Command
  final case class StartWorkflow(
                                address: String,
                                details: String,
                                userId: String
                                ) extends Command
  def apply(zeebeClient: ZeebeClient): Behavior[Command] ={
    Behaviors.setup(context => new Client(context, zeebeClient))
  }
}

class Client(context:ActorContext[Command], zeebeClient: ZeebeClient) extends AbstractBehavior[Command](context){
  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case StartWorkflow(address, details, userId) =>
        context.system.log
            .info("creating workflow instance")
        zeebeClient.newCreateInstanceCommand()
          .bpmnProcessId("delivery-process")
          .latestVersion()
          .variables(Map("address"->address, "details"->details, "userId"->userId).asJava)
          .send().join()
        Behaviors.stopped
    }
  }
}
