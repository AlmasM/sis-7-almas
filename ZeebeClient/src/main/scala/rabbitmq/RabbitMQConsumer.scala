package rabbitmq



import akka.stream.alpakka.amqp._
import akka.stream.alpakka.amqp.scaladsl.{AmqpSource, CommittableReadResult}
import akka.stream.scaladsl.GraphDSL.Implicits._
import akka.stream.scaladsl.{GraphDSL, RestartSource, RunnableGraph, Sink, Source}
import akka.stream.{ClosedShape, Materializer, Outlet}
import akka.{Done, NotUsed}
import io.zeebe.client.ZeebeClient
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

abstract class RabbitMQConsumer(topicName: String, reconnectionDelay: FiniteDuration, qos: Int)(
  implicit materializer: Materializer,
  connection: AmqpConnectionProvider
) extends Serializers {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  /**
   * Routing exchange parameters
   */
  protected val routingExchangeDeclaration: ExchangeDeclaration =
    ExchangeDeclaration(topicName, "topic")
      .withDurable(true)
      .withAutoDelete(false)
      .withInternal(true)

  /**
   * Queue parameters
   */
  protected def queueDeclaration: QueueDeclaration

  protected def declarations: Seq[Declaration]

  protected val amqpSource: Source[CommittableReadResult, NotUsed] =
    RestartSource.withBackoff(reconnectionDelay, 10 * reconnectionDelay, 0) { () =>
      AmqpSource.committableSource(
        NamedQueueSourceSettings(connection, queueDeclaration.name)
          .withDeclarations(declarations.toList),
        bufferSize = qos
      )
    }

  /**
   * Run Consumer
   * @param busConsumerSink - Sink for Consumer output
   *
   * @return
   */
  private def run(busConsumerSink: Sink[CommittableReadResult, Future[Done]]): Future[Done] = {

    // SourceShape - like a Configurable rule for Amqp connection
    val rabbitIncomingSource = RunnableGraph.fromGraph(GraphDSL.create(busConsumerSink) { implicit builder => sink =>
      // builder add source stage
      val amqpOut: Outlet[CommittableReadResult] = builder.add(amqpSource).out
      // mat
      amqpOut ~> sink.in
      // Make closed graph
      ClosedShape
    })

    // run create reference consumer graph
    rabbitIncomingSource.run()
  }

  def startConsume(zeebeClient: ZeebeClient): Future[Done] =
    run(Sink.foreach { message =>
      logger.info(s"CommittableReadResult : ${message.message.bytes.utf8String}")

      zeebeClient.newCreateInstanceCommand().bpmnProcessId("delivery-process").
        latestVersion().
        variables(message.message.bytes.utf8String).
        withResult().send().join()

      message.ack()
    })

}

