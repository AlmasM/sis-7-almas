package rabbitmq


import Domain.Consume.ConsumeBody
import akka.http.scaladsl.model.ErrorInfo
import org.json4s.{ShortTypeHints, jackson}
import org.json4s.jackson.Serialization

trait Serializers {

  implicit val formats = Serialization.formats(
    ShortTypeHints(
      List(
        classOf[ConsumeBody],
        classOf[ErrorInfo]
      )
    )
  )
  implicit val serialization = jackson.Serialization
}

