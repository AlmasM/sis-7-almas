package Domain

object Consume {
  case class ConsumeBody(
    address: String,
    details: String,
    userId: String
  )
}
