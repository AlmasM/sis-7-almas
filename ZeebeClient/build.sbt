name := "DeliveryHandle2"

version := "0.1"

scalaVersion := "2.13.3"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.0"
val alpakkaVersion = "1.1.2"
val json4sVersion = "3.6.7"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "io.zeebe" %"zeebe-client-java" % "0.24.1",
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-slf4j"       % AkkaVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.lightbend.akka"         %% "akka-stream-alpakka-amqp" % alpakkaVersion,
  "org.json4s"                 %% "json4s-jackson" % json4sVersion,
  "org.json4s"                 %% "json4s-native" % json4sVersion
)