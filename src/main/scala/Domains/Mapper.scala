package Domains

import cats.syntax.functor._
import io.circe.bson.BsonCodecInstances
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}


sealed trait Domain

case class CreateRequest(
                          address: String,
                          details:String,
                          userId:String
                        ) extends Domain
case class Order (
                   id:String,
                   address:String,
                   details:String,
                   userId:String,
                   distance: Option[Float]
                 ) extends Domain
case class User(
               id:String,
               phone: String,
               lastName: String,
               firstName: String
               ) extends Domain
case class StartWorkflowRequest(
                                 address:String,
                                 details:String,
                                 userId: String
                               ) extends Domain

trait MainCodec extends BsonCodecInstances  {
  implicit val encodeDomainEntity: Encoder[Domain] = Encoder.instance {
    case e: CreateRequest => e.asJson
    case e: Order => e.asJson
    case e: StartWorkflowRequest => e.asJson
    case e: User => e.asJson
  }

  implicit val decodeDomainEntity: Decoder[Domain] =
    List[Decoder[Domain]](
      Decoder[Domain].widen
    ).reduceLeft(_.or(_))
}