package rabbitmq

import akka.NotUsed
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.stream.{Materializer, OverflowStrategy, QueueOfferResult}
import akka.stream.alpakka.amqp.{AmqpConnectionProvider, AmqpWriteSettings, ExchangeDeclaration, WriteMessage}
import akka.stream.alpakka.amqp.scaladsl.AmqpSink
import akka.stream.scaladsl.{RestartSink, RunnableGraph, Sink, Source, SourceQueueWithComplete}
import akka.util.ByteString
import com.rabbitmq.client.AMQP.BasicProperties
import org.json4s.native.Serialization.write
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.{FiniteDuration, _}
import scala.jdk.CollectionConverters._


sealed trait AmqpProducerCommand

case class ProduceMessage(
                           body: AnyRef,
                           routingKey: String,
                           correlationId: String,
                           replyTo: Option[String],
                           headers: Map[String, AnyRef]
                         ) extends AmqpProducerCommand


object RabbitMQProducer  extends Serializers {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)


  private def createAmqpSink(exchangeName: String, exchangeType: String, reconnectionDelay: FiniteDuration = 1.seconds)(
    implicit materializer: Materializer,
    connection: AmqpConnectionProvider
  ): Sink[WriteMessage, NotUsed] = {
    val exchangeDeclaration =
      ExchangeDeclaration(exchangeName, exchangeType)
        .withDurable(true)

    RestartSink.withBackoff(reconnectionDelay, 10 * reconnectionDelay, 0, 600) { () =>
      AmqpSink(
        AmqpWriteSettings(connection)
          .withExchange(exchangeDeclaration.name)
          .withDeclaration(exchangeDeclaration)
      )
    }
  }
  def apply(reconnectionDely: FiniteDuration, exchangeName: String, exchangeType: String)(
    implicit
    materializer: Materializer,
    connection: AmqpConnectionProvider
  ): Behavior[AmqpProducerCommand] = Behaviors.setup { ctx =>
    val amqpSink: Sink[WriteMessage, NotUsed] = createAmqpSink(exchangeName, exchangeType, reconnectionDely)
    import ctx.executionContext

    val streamQueue: SourceQueueWithComplete[WriteMessage] = {
      val rabbitOutgoingSource: Source[WriteMessage, SourceQueueWithComplete[WriteMessage]] =
        Source.queue[WriteMessage](30, OverflowStrategy.backpressure)

      val graph: RunnableGraph[SourceQueueWithComplete[WriteMessage]] =
        rabbitOutgoingSource.to(amqpSink)

      graph.run()
    }

    Behaviors.receiveMessage {
      case ProduceMessage(msgBody, routingKey, correlationId, replyTo, headers) =>
        val body = write(msgBody)
        logger.info(s"Json for RabbitMQ is: $body")
        val properties = new BasicProperties.Builder()
          .contentType("application/json")
          .replyTo(replyTo.orNull)
          .correlationId(correlationId)
          .headers(headers.asJava)
          .build()

        val writeMessage = WriteMessage(ByteString(body), immediate = false, mandatory = false)
          .withRoutingKey(routingKey)
          .withMandatory(false)
          .withImmediate(false)
          .withProperties(properties)

        streamQueue.offer(writeMessage).onComplete {
          case util.Success(value: QueueOfferResult) =>
            logger.info("Successfully pushed to rabbitMQ", value.toString)
            Behaviors.stopped
          case util.Failure(exception) =>
            exception.printStackTrace()
            logger.error("Failed to push to rabbitMQ", exception.getMessage)

            Behaviors.stopped
        }
        Behaviors.stopped
    }
  }


}
