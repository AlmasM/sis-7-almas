package rabbitmq


import Actors.OrderActor.ProduceRequest
import Domains.User
import akka.http.scaladsl.model.ErrorInfo
import org.json4s.{ShortTypeHints, jackson}
import org.json4s.jackson.Serialization

trait Serializers {

  implicit val formats = Serialization.formats(
    ShortTypeHints(
      List(
        classOf[User],
        classOf[ProduceRequest],
        classOf[ErrorInfo]
      )
    )
  )
  implicit val serialization = jackson.Serialization
}

