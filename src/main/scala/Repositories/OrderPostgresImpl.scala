package Repositories
import Domains.Order
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future

class OrderPostgresImpl(db: Database, val orderPostgres: OrderPostgres) {
  def insertOrder(order: Order): Future[Order] = {
    val insert = orderPostgres.insert(order)
    db.run(insert)
  }
  def updateOrder(orderId: String, distance:Option[Float]):Future[Int] = {
    val update = orderPostgres.update(orderId, distance)
    db.run(update)
  }
  def selectOrder(orderId: String) ={
    val select = orderPostgres.select(orderId)
    db.run(select)
  }
  def deleteOrder(orderId: String): Future[Int] ={
    val delete = orderPostgres.delete(orderId)
    db.run(delete)
  }
}
