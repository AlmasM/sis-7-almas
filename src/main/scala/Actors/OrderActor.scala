package Actors

import java.util.UUID.randomUUID

import Actors.OrderActor._
import Domains.{Order, StartWorkflowRequest}
import Repositories.OrderPostgresImpl
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.Materializer
import akka.stream.alpakka.amqp.AmqpConnectionProvider
import com.typesafe.config.Config
import io.circe.generic.auto._
import io.circe.syntax._
import rabbitmq.{AmqpProducerCommand, ProduceMessage, RabbitMQProducer}

import scala.concurrent.duration._
import scala.util.{Failure, Random, Success}

object OrderActor{
  sealed trait Command
  final case class CreateOrder(
                               address:String,
                               details: String,
                               userId: String,
                               replyTo: ActorRef[Either[ErrorInfo, Order]]
                              ) extends Command
  final case class OptimizeOrderDistance(
                                     orderId: String,
                                     replyTo: ActorRef[Either[ErrorInfo, String]]
                                   ) extends Command
  final case class RemoveOrder(
                                orderId: String,
                                replyTo: ActorRef[Either[ErrorInfo, String]]
                              ) extends Command
  final case class GetOrder(
                             orderId: String,
                             replyTo: ActorRef[Either[ErrorInfo, Order]]
                           ) extends Command
  final case class StartWorkflow(
                                  address:String,
                                  details:String,
                                  userId:String,
                                  replyTo: ActorRef[Either[ErrorInfo, HttpResponse]]
                                ) extends Command
  final case class ProduceRequest(
                                  address: String,
                                  details: String,
                                  userId: String
                                 ) extends Command

  def apply(config: Config, orderPostgresImpl: OrderPostgresImpl)
           (implicit materializer: Materializer, connection:
            AmqpConnectionProvider): Behavior[Command] = {
    Behaviors.setup(context => new OrderActor(context, config, orderPostgresImpl).receive())
  }
}

class OrderActor(context: ActorContext[Command], config: Config, orderPostgresImpl: OrderPostgresImpl)
                (implicit materializer: Materializer,
                 connection: AmqpConnectionProvider){
  implicit val system = context.system
  implicit val executionContext = system.executionContext

  def sendRequest(address:String, details:String, userId: String) = {
    system.log
        .info("sending request to start work flow")
    Http().singleRequest(HttpRequest(
        method = HttpMethods.POST,
        uri = Uri(config.getString("zeebeClient.startWorkflowUri")),
        entity = HttpEntity(
          ContentTypes.`application/json`,
          StartWorkflowRequest(address, details, userId).asJson.noSpaces
        )
      )
    )
  }

  def generateOptimizedDistance(orderId: String): Float = {
    val distance = Random.between(0f, 4f) // assume here some interesting algorithm
    system.log
        .info(s"generated optimized distance $distance for order with id=$orderId")
    distance
  }

  def receive(): Behavior[Command] = {
    Behaviors.receiveMessage{
      case createOrder: CreateOrder =>
        orderPostgresImpl.insertOrder(
          Order(
            id = randomUUID().toString,
            createOrder.address,
            createOrder.details,
            createOrder.userId,
            None
          )
        ) onComplete {
          case Success(order) =>
            createOrder.replyTo ! Right(
              order
            )
          case Failure(exception) =>
            createOrder.replyTo ! Left(
              new ErrorInfo(exception.getMessage)
            )
        }
        system.log
        .info("sending insert to db")
        Behaviors.same
      case optimizeOrderDistance: OptimizeOrderDistance =>
        orderPostgresImpl.updateOrder(
          optimizeOrderDistance.orderId,
          Some(generateOptimizedDistance(optimizeOrderDistance.orderId))) onComplete {
          case Success(_) =>
            optimizeOrderDistance.replyTo ! Right(
              "ok"
            )
          case Failure(exception) =>
            optimizeOrderDistance.replyTo ! Left(
              new ErrorInfo(exception.getMessage)
            )
        }
        Behaviors.same
      case removeOrder: RemoveOrder =>
        orderPostgresImpl.deleteOrder(removeOrder.orderId) onComplete {
          case Success(_) =>
            removeOrder.replyTo ! Right(
              "ok"
            )
          case Failure(exception) =>
            removeOrder.replyTo ! Left(
              new ErrorInfo(exception.getMessage)
            )
        }
        Behaviors.same
      case getOrder: GetOrder =>
        orderPostgresImpl.selectOrder(getOrder.orderId) onComplete {
          case Success(value) if value.nonEmpty =>
            getOrder.replyTo ! Right(value.head)
          case _ =>
            getOrder.replyTo ! Left(
              new ErrorInfo("Something is totally wrong")
            )
        }
        Behaviors.same
      case startWorkflow: StartWorkflow =>
        sendRequest(startWorkflow.address, startWorkflow.details, startWorkflow.userId) onComplete {
          case Success(response) =>
            startWorkflow.replyTo ! Right(response)
          case Failure(_) =>
            startWorkflow.replyTo ! Left(
              new ErrorInfo(summary = "Something bad happened")
            )
        }
        Behaviors.same
      case produceRequest: ProduceRequest =>

        val produceMessage = ProduceMessage(
          produceRequest,
          "request.app.create-user",
          context.self.path.toStringWithoutAddress,
          None,
          Map()
        )
        val producer: ActorSystem[AmqpProducerCommand] =
          ActorSystem(RabbitMQProducer.apply(10.seconds, "X:gateway.out.fanout", "fanout"), "producer")
        producer ! produceMessage
        Behaviors.same
      case _ =>
        system.log.info("Unexpected Message!")
        Behaviors.same
    }
  }
}
