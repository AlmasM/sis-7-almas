import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.{Http, server}
import akka.http.scaladsl.server.RouteConcatenation
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}
import scala.concurrent.duration._

class HttpServer(route: server.Route, system: ActorSystem[_], config: Config) extends RouteConcatenation{

  implicit val classicActorSystem: akka.actor.ActorSystem = system.classicSystem
  implicit val executionContext: ExecutionContext = system.executionContext
  private val shutdown = CoordinatedShutdown(system)
  def start(): Unit = {
    Http().newServerAt(config.getString("app.host"), config.getInt("app.port")).bind(route).onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log
            .info(s"server is online at ${address.getHostName} ${address.getPort}")
        shutdown.addTask(CoordinatedShutdown.PhaseServiceRequestsDone, "http-graceful-terminate") { () =>
          binding.terminate(10.seconds).map { _ =>
            system.log
              .info("DAR-api http://{}:{}/ graceful shutdown completed", address.getHostString, address.getPort)
            Done
          }
        }
      case Failure(_) =>
        system.terminate()
    }
  }
}
