package Routes

import Actors.OrderActor
import Actors.OrderActor._
import Domains.{CreateRequest, Order}
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server
import akka.http.scaladsl.server.Directives._
import io.circe.generic.auto._
import io.circe.syntax._
import spray.json.DefaultJsonProtocol._

import scala.concurrent.Future
import scala.util.{Failure, Success}


class OrderRoute(val actor: ActorRef[OrderActor.Command])(implicit val system: ActorSystem[_])
  extends Route {
  implicit val requestBodyFormatter = jsonFormat3(CreateRequest)
  private def createOrder(body: CreateRequest): Future[Either[ErrorInfo, Order]] = {
    actor.ask(CreateOrder(body.address, body.details, body.userId, _))
  }
  private def optimizeDistance(orderId: String): Future[Either[ErrorInfo, String]] = {
    actor.ask(OptimizeOrderDistance(orderId,_ ))
  }
  private def deleteOrder(orderId: String): Future[Either[ErrorInfo, String]] = {
    actor.ask(RemoveOrder(orderId, _))
  }
  private def getOrder(orderId: String): Future[Either[ErrorInfo, Order]] = {
    actor.ask(GetOrder(orderId, _))
  }
  private def startWorkflow(address:String, details:String, userId:String): Future[Either[ErrorInfo, HttpResponse]] = {
    actor.ask(StartWorkflow(address, details, userId, _))
  }
  private def produceCreateOrder(body: CreateRequest): Unit = {
    actor ! ProduceRequest(body.address, body.details, body.userId)
  }
  val route = concat(
    post{
      pathEndOrSingleSlash {
        entity(as[CreateRequest]){ body =>
          onComplete(createOrder(body)){
            case Success(value) =>
              value.toOption match {
                case Some(order) =>
                  system.log.info(s"created order with id=${order.id}")
                  complete(HttpEntity(ContentTypes.`application/json`, order.asJson.noSpaces))
                case _ =>
                  complete(StatusCodes.BadRequest)
              }
            case Failure(_) =>
              complete(StatusCodes.InternalServerError)
          }
        }
      }
    },
    post{
      path("optimize-distance") {
        parameter("orderId") { orderId =>
          onComplete(optimizeDistance(orderId)) {
            case Success(value) =>
              value.toOption match {
                case Some(value) =>
                  complete(value)
                case _ =>
                  complete(StatusCodes.BadRequest)
              }
            case Failure(_) =>
              complete(StatusCodes.InternalServerError)
          }
        }
      }
    },
    delete{
      parameter("orderId"){ orderId =>
        onComplete(deleteOrder(orderId)){
          case Success(value) =>
            value.toOption match {
              case Some(value) =>
                complete(value)
              case _ =>
                complete(StatusCodes.BadRequest)
            }
          case Failure(_) =>
            complete(StatusCodes.InternalServerError)
        }
      }
    },
    get{
      parameter("orderId"){ orderId =>
        onComplete(getOrder(orderId)){
          case Success(value) =>
            value.toOption match {
              case Some(order) =>
                complete(HttpEntity(ContentTypes.`application/json`, order.asJson.noSpaces))
              case _ =>
                complete(StatusCodes.NoContent)
            }
          case Failure(_) =>
            complete(StatusCodes.InternalServerError)
        }
      }
    },
    post{
      path("startWorkflow") {
        entity(as[CreateRequest]) { body =>
        onComplete(startWorkflow(body.address, body.details, body.userId)) {
          case Success(_) =>
            complete(StatusCodes.Accepted)
          case Failure(_) =>
            complete(StatusCodes.InternalServerError)
           }
        }
      }
    },
    post{
      path("create-async") {
        entity(as[CreateRequest]) {
          body =>
            produceCreateOrder(body)
            complete(StatusCodes.Accepted)
        }
      }
    }
  )
  val orderRoute: server.Route = pathPrefix("order"){
    concat(
      route
    )
  }
}
