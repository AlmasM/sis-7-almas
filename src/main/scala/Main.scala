import Actors.OrderActor
import Repositories.{OrderPostgres, OrderPostgresImpl}
import Routes.OrderRoute
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.server
import akka.stream.alpakka.amqp.AmqpConnectionProvider
import com.typesafe.config.{Config, ConfigFactory}
import migrations.Migration
import rabbitmq.RabbitMQConnector
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext

object Main extends RabbitMQConnector {
  def startHttpServer(route: server.Route, system: ActorSystem[_], config: Config): Unit = {
    new HttpServer(route, system, config).start()
  }
  def main(args: Array[String]): Unit = {
    val rootBehavior = Behaviors.setup[Nothing] {
      context =>
        implicit val system = context.system
        implicit val executionContext: ExecutionContext = context.system.executionContext

        val config = ConfigFactory.load()

        //db
        val orderDb = Database.forConfig("dbPostgres.users")
        val orderPostgres = new OrderPostgres()
        val orderPostgresImpl = new OrderPostgresImpl(orderDb, orderPostgres)
        //rabbit
        implicit val amqpConnectionProvider: AmqpConnectionProvider = createAmqpConnection(config)

        //actor
        val orderActor = context.spawn(OrderActor(config, orderPostgresImpl), "Actors.OrderActor")
        Migration.migrate()
        system.log.info("server is starting")
        val orderRoute = new OrderRoute(orderActor).orderRoute
        startHttpServer(orderRoute, system, config)
        Behaviors.empty
    }
    ActorSystem[Nothing](rootBehavior, "main")
  }
}