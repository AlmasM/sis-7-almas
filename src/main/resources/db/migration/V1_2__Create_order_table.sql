CREATE TABLE "orders" (
  "id"      VARCHAR PRIMARY KEY,
  "address" VARCHAR NOT NULL,
  "details" VARCHAR NOT NULL,
  "userId"  VARCHAR NOT NULL,
  "distance" FLOAT
);
